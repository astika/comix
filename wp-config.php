<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи и ABSPATH. Дополнительную информацию можно найти на странице
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется скриптом для создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения вручную.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wordpress');

/** Имя пользователя MySQL */
define('DB_USER', 'tester');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'madhumangal');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         '*mPs>J!Wr(r 4WCGt=5A1r&COy67B-g:FBSSe?=5s`Qe|:>a^!Hf|%}U|y=A2pX=');
define('SECURE_AUTH_KEY',  'vM6-,|oV6_((+wnL<LeSc_Mg|THY7M5O,b3sy^SC0UtYd&91+D5.qCy5/KGny9m6');
define('LOGGED_IN_KEY',    'F8`P|1Ua?])y=0zP5X5B+3g>6lVxo]97]N?#:r27`Co6l*#1q|(ariP7VsC+sIy>');
define('NONCE_KEY',        '79r :cX:t`XqAS][{]gmQty7&^D|,O<h5Dc8rLg3$yu*>&MR*h7*OF7WZaJX{y>-');
define('AUTH_SALT',        'PMY<j/(YXlWzz+x_==K)bIb>jiU|kc/I`5,Ewzc/OIXA++=(V6vz<wb`#cHag&h&');
define('SECURE_AUTH_SALT', 'Wh@O0/5yq:/a&^<<<J2y.f=yxi)>mm3u2X?rOaD_#FU_jVE8r>-[{IugM&(XH?r!');
define('LOGGED_IN_SALT',   '{Kbt_3p_ok97}a]h[(|_6T.ps|-<1X3E_|_ vY!@k5Q0w6^<1]JqatgQV,Sr}fG?');
define('NONCE_SALT',       'p+TGb>e>D+]c[Ay&-6ni3w1OL7/O$-U*Y-:3)||==$@D/S+BWv&er6?Kw4MN^jlo');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'mhb_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
